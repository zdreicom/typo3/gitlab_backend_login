<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'GitLab Backend-Login',
    'description' => 'Add support to login to TYPO3 Backend via GitLab',
    'category' => 'services',
    'author' => 'zdreicom AG',
    'author_email' => 'info@zdrei.com',
    'author_company' => 'zdreicom AG',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'version' => '0.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.5-10.4.99'
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
